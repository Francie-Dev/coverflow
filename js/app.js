$(document).ready(function(){
    const options = {
        id: '#row',
        slider: '#slide',
        prev: '#prev',
        next: '#next',
        number: 1,
        largeur: 18500,
        index: 0,
        speed: 1000,
        easing: 'linear'
    }
    slider = new slider(options);
});

var slider = function(element){
    var self=this;
    this.elem = $(element.id);
    this.index = element.index;
    this.largeur = element.largeur;
    this.largeurDiv = this.elem.width();
    this.prev = self.elem.find(element.prev);
    this.next = self.elem.find(element.next);
    this.slider = this.elem.find(element.slider);

    /*    
        this.elem.find('a').each(function(){ 
            self.largeur+= $(this).width();
        });
    */

    this.saut = this.largeurDiv;
    this.nombreEtapes = Math.ceil(this.largeur/this.saut)-(this.largeurDiv/this.saut);

    this.prev.hide();
    if (this.largeurDiv>this.largeur) {this.next.hide();}
    this.current = $('#' + this.index).addClass( "current" ).removeClass( "dots" );

    removeCurrent = () =>{
        $('.current').removeClass( "current" ).addClass( "dots" );
    }

    addCurrent = () => {
        $('#' + self.index).addClass( "current" ).removeClass( "dots" );
    }

    next = () => {
        if (self.index >= self.nombreEtapes) {return false;}
        if (self.index==0) {
            this.prev.fadeIn();
        }
        removeCurrent();
        self.index++;
        addCurrent();
        self.slider.animate({
           'left': -self.index*self.saut
       }, element.speed, element.linear)
        if (self.index >= self.nombreEtapes) {
            self.next.fadeOut()
        }
    };

    prev = () => {   
        if (self.index<=0) {return false;}
        if (self.index>=self.nombreEtapes) {
            this.next.fadeIn();
        } 
        removeCurrent();
        self.index--;
        addCurrent();
        self.slider.animate({
            'left': -self.index*self.saut
        }, element.speed, element.linear)
        if (self.index==0) {
            self.prev.fadeOut()
        }
    };

    
    pagination = (num) => {
        if (num==0) {
            self.prev.fadeOut();
        }
        else{
            self.prev.fadeIn();
        }
        if (num==self.nombreEtapes) {
            self.next.fadeOut();
        }
        else {
            self.next.fadeIn();
        }
        removeCurrent();
        self.elem.find(element.slider).animate({
            'left': -self.saut*num
        }, element.speed, element.linear);
        self.index=num;
        addCurrent();
    };

    console.log(`Taille du saut - ` + this.saut);
    console.log(`largeur div - ` + this.largeurDiv + `px`);
    console.log(`Taille de la div - ` + this.largeur + `px`);
    console.log(`Nombres d'etapes possible - ` + this.nombreEtapes);
}